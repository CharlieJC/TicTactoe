package me.charlie.TicTacToeOld;

/**
 * Created by Charlie on 2/22/2017.
 */
class Point {
    private final int X;
    private final int Y;

    Point(int x, int y) {
        this.X = x;
        this.Y = y;
    }

    int getX() {
        return X;
    }

    int getY() {
        return Y;
    }
}
