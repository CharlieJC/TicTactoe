package me.charlie.TicTacToeOld;

import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static me.charlie.TicTacToeOld.TicTacToeGUI.Player.*;


/**
 * Created by Charlie on 2/20/2017.
 */
class TicTacToeGUI extends JFrame {
    private List<AIMove> moves = new ArrayList<>();
    private Container pane;
    private Player currentPlayer;
    private Player[][] board;
    private TTTButton[][] buttonGrid;
    private boolean hasWinner;
    private int gridSize;
    private int turn;
    private JLabel messageBar;
    private Opponent opponent;
    private List<PointsAndScores> rootsChildrenScores;

    TicTacToeGUI(Opponent opponent) {
        super("Tic Tac Toe");
        this.gridSize = 3;
        this.turn = 0;
        this.pane = this.getContentPane();
        this.setLayout(new GridLayout(4, 3));
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setVisible(true);
        this.currentPlayer = X;
        this.board = new Player[gridSize][gridSize];
        this.buttonGrid = new TTTButton[gridSize][gridSize];
        this.hasWinner = false;
        this.messageBar = new JLabel("Tic Tac Toe");
        this.messageBar.setHorizontalAlignment(SwingConstants.RIGHT);
        this.messageBar.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        this.initBoard();
        this.initMenuBar();
        this.opponent = opponent;
    }

    private void initMenuBar() {

        JMenuBar menuBar;
        JMenu menu;
        JMenuItem quit;
        JMenuItem newGame;

        menuBar = new JMenuBar();
        menu = new JMenu("File");
        newGame = new JMenuItem("New Game");
        quit = new JMenuItem("Quit");

        newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetBoard();
            }
        });

        quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        menu.add(newGame);
        menu.add(quit);
        menuBar.add(menu);
        this.setJMenuBar(menuBar);
    }

    private void resetBoard() {
        sendMessage("Game Reset!");
        this.currentPlayer = X;
        this.hasWinner = false;
        this.turn = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.buttonGrid[i][j].setText("");
                this.buttonGrid[i][j].setBackground(new Color(27, 119, 255));
                this.buttonGrid[i][j].setBorder(new BevelBorder(BevelBorder.LOWERED));

                this.board[i][j] = EMPTY;
            }
        }
    }

    private void initBoard() {
        pane.setBackground(new Color(27, 119, 255));
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {

                if (i < 3) {
                    TTTButton btn = new TTTButton(i, j);
                    btn.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));
                    btn.setBackground(new Color(27, 119, 255));
                    btn.setBorder(new BevelBorder(BevelBorder.LOWERED));
                    this.buttonGrid[i][j] = btn;
                    this.board[i][j] = EMPTY;

                    btn.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (((JButton) e.getSource()).getText().equals("") && (!hasWinner)) {
                                btn.setText(currentPlayer.name());
                                btn.setBackground(getPlayerColour(currentPlayer));
                                btn.setBorder(new BevelBorder(BevelBorder.RAISED));
                                playerMove(btn.getPosX(), btn.getPosY());
                                togglePlayer();
                            }
                        }
                    });

                    pane.add(btn);
                } else {
                    if (j == 2) {
                        sendMessage("Player X begins!");
                    }
                }
            }
        }
    }

    private void sendMessage(String str) {
        messageBar.setForeground(new Color(255, 255, 255));
        messageBar.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
        messageBar.setText(str);
        pane.add(messageBar, 9);

    }

    private void togglePlayer() {
        if (currentPlayer == X) {
            switch (opponent) {
                case PLAYER:
                    this.currentPlayer = O;
                    break;
                case BOT_EASY:
                    this.currentPlayer = O;
                    this.easyAIMove();
                    break;
                case BOT_NORMAL:
                    this.currentPlayer = O;
                    this.normalAIMove();
                    break;
                case BOT_HARD:
                    this.currentPlayer = O;
                    this.hardAIMove();
                    break;
            }
        } else {
            this.currentPlayer = X;
        }
    }

    private void easyAIMove() {
        int randomNum = ThreadLocalRandom.current().nextInt(0,4);
        if (randomNum > 3) {
            hardAIMove();
        } else {
            randomAIMove();
        }
    }

    private void normalAIMove() {
        int randomNum = ThreadLocalRandom.current().nextInt(0,4);
        if (randomNum > 3) {
            randomAIMove();
        } else {
            hardAIMove();
        }
    }

    private void randomAIMove() {
        int randomX = ThreadLocalRandom.current().nextInt(0, 3);
        int randomY = ThreadLocalRandom.current().nextInt(0, 3);

        if (board[randomX][randomY] == EMPTY) {
            buttonGrid[randomX][randomY].doClick();
        } else if (!hasWinner) {
            normalAIMove();
        }
    }

    private void playerMove(int x, int y) {
        Player player = currentPlayer;
        if (board[x][y] == EMPTY || board[x][y] == null) {
            board[x][y] = player;
        }

        turn++;

        //check column
        for (int i = 0; i < gridSize; i++) {
            if (board[x][i] != player) break;

            if (i == gridSize - 1) {
                end(player);
            }
        }

        //check row
        for (int i = 0; i < gridSize; i++) {
            if (board[i][y] != player) break;

            if (i == gridSize - 1) {
                end(player);
            }
        }

        if (x == y) {
            for (int i = 0; i < gridSize; i++) {
                if (board[i][i] != player) break;

                if (i == gridSize - 1) {
                    end(player);
                }
            }
        }

        if (x + y == gridSize - 1) {
            for (int i = 0; i < gridSize; i++) {
                if (board[i][(gridSize - 1) - i] != player) break;

                if (i == gridSize - 1) {
                    end(player);
                }
            }
        }

        if (this.turn == 9 && !hasWinner) {
            end(EMPTY);
        }

    }

    private void end(Player player) {
        switch (player) {
            case X:
                sendMessage("Player " + X.name() + " won!");
                this.hasWinner = true;
                break;
            case O:
                sendMessage("Player " + O.name() + " won!");
                this.hasWinner = true;
                break;
            case EMPTY:
                sendMessage("You both suck!");
                this.hasWinner = true;
                break;
        }

    }

    private List<Point> getAvaliablePoints() {
        List<Point> avaliablePoints = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == EMPTY) {
                    avaliablePoints.add(new Point(i, j));
                }
            }
        }
        return avaliablePoints;
    }

    private void hardAIMove() {

        if (hasWinner) return;

        callMinimax(0, O);

        for (PointsAndScores pas : rootsChildrenScores) {
            System.out.println("Point: " + pas.getPoint() + " Score: " + pas.getScore());
        }
        Point point = returnBestMove();

        buttonGrid[point.getX()][point.getY()].doClick();
//
//        AIMove bestMove = getBestMove(currentPlayer);
//        buttonGrid[bestMove.x][bestMove.y].doClick();


    }

    private int returnMin(List<Integer> list) {
        int min = Integer.MAX_VALUE;
        int index = -1;
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i) < min) {
                min = list.get(i);
                index = i;
            }
        }
        return list.get(index);
    }

    private int returnMax(List<Integer> list) {
        int max = Integer.MIN_VALUE;
        int index = -1;
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i) > max) {
                max = list.get(i);
                index = i;
            }
        }
        return list.get(index);
    }

    private void callMinimax(int depth, Player turn) {
        rootsChildrenScores = new ArrayList<>();
        minimax(depth, turn);
    }

    private Point returnBestMove() {
        int MAX = -100000;
        int best = -1;

        for (int i = 0; i < rootsChildrenScores.size(); ++i) {
            if (MAX < rootsChildrenScores.get(i).getScore()) {
                MAX = rootsChildrenScores.get(i).getScore();
                best = i;
            }
        }

        return rootsChildrenScores.get(best).getPoint();
    }

    private int minimax(int depth, Player player) {

        List<Point> pointsAvailable = getAvaliablePoints();


        if (hasXWon()) {
            return -1;
        } else if (hasOWon()) {
            return 1;
        } else if (pointsAvailable.isEmpty()) {
            return 0;
        }

        List<Integer> scores = new ArrayList<>();

        for (int i = 0; i < pointsAvailable.size(); ++i) {
            Point point = pointsAvailable.get(i);

            if (player == O) { //X's turn select the highest from below minimax() call
                placeAMoveAI(point, O);
                int currentScore = minimax(depth + 1, X);
                scores.add(currentScore);

                if (depth == 0)
                    rootsChildrenScores.add(new PointsAndScores(currentScore, point));

            } else if (player == X) {//O's turn select the lowest from below minimax() call
                placeAMoveAI(point, X);
                scores.add(minimax(depth + 1, O));
            }
            board[point.getX()][point.getY()] = EMPTY; //Reset this point
        }
        return player == O ? returnMax(scores) : returnMin(scores);
    }

    private void placeAMoveAI(Point point, Player player) {
        board[point.getX()][point.getY()] = player;
    }

    @Nullable
    private Color getPlayerColour(Player player) {
        switch (player) {
            case X:
                return new Color(0, 255, 0);
            case O:
                return new Color(255, 0, 0);
            default:
                return null;
        }
    }

    private boolean hasXWon() {
        if ((board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0] == X) || (board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[0][2] == X)) {
            return true;
        }

        for (int i = 0; i < 3; ++i) {
            if (((board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] == X)
                    || (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] == X))) {
                return true;
            }
        }
        return false;
    }

    private boolean hasOWon() {
        if ((board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0] == O) || (board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[0][2] == O)) {
            return true;
        }
        for (int i = 0; i < 3; ++i) {
            if ((board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] == O)
                    || (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] == O)) {
                return true;
            }
        }

        return false;
    }

    private AIMove getBestMove(Player player) {

        //Base case, check for end state
        if (hasOWon()) {
            AIMove move = new AIMove();
            move.score = 10;
            return move;
        } else if (hasXWon()) {
            AIMove move = new AIMove();
            move.score = -10;
            return move;
        } else if (isTie()) {
            AIMove move = new AIMove();
            move.score = 0;
            return move;
        }

        //Do the recursive function calls, construct the moves list
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == EMPTY) {
                    AIMove move = new AIMove();
                    move.x = i;
                    move.y = j;

                    board[i][j] = player;

                    if (player == O) {
                        move.score = getBestMove(X).score;
                    } else {
                        move.score = getBestMove(O).score;
                    }

                    moves.add(move);

                    board[i][j] = EMPTY;

                }
            }
        }

        int bestMove = 0;
        if (player == O) {
            int bestScore = -1000000;
            for (int i = 0; i < moves.size(); i++) {
                if (moves.get(i).score > bestScore) {
                    bestMove = i;
                    bestScore = moves.get(i).score;
                }
            }
        } else {
            int bestScore = 1000000;
            for (int i = 0; i < moves.size(); i++) {
                if (moves.get(i).score < bestScore) {
                    bestMove = i;
                    bestScore = moves.get(i).score;
                }
            }
        }
        return moves.get(bestMove);

    }

    private int playerToInt(Player player) {

        switch (player) {
            case X:
                return 1;
            case O:
                return 2;
            case EMPTY:
                return 0;
        }
        return 0;
    }

    private boolean isTie() {
        int occupiedSlots = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] != EMPTY) {
                    occupiedSlots++;
                }
            }
        }

        return occupiedSlots == 9;
    }

    enum Player {
        X, O, EMPTY
    }

    enum Opponent {
        PLAYER, BOT_EASY, BOT_NORMAL, BOT_HARD, ONLINE
    }

    private class AIMove {
        int x;
        int y;
        int score;
    }
}


