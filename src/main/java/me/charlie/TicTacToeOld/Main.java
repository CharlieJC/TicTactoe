package me.charlie.TicTacToeOld;

import javax.swing.*;

/**
 * Created by Charlie on 2/20/2017.
 */
public class Main {
    public static void main(String[] args) {
        launchOptions();
    }

    private static void launchOptions() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new PickOpponent();
            }
        });
    }

    static void launchGame(TicTacToeGUI.Opponent opponent) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TicTacToeGUI(opponent);
            }
        });
    }
}
