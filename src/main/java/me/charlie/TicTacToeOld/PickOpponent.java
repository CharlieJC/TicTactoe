package me.charlie.TicTacToeOld;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Charlie on 2/22/2017.
 */
class PickOpponent extends JFrame {

    private Container pane;
    private JButton player2;
    private JButton botEasy;
    private JButton botNormal;
    private JButton botHard;

    PickOpponent() {

        super("Choose your opponent");
        this.pane = this.getContentPane();
        this.setLayout(new FlowLayout());
        this.setSize(300, 100);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setVisible(true);
        pane.setBackground(new Color(27, 119, 255));

        initButtons();

    }

    private void initButtons() {
        this.player2 = new JButton("Player 2");
        this.player2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.launchGame(TicTacToeGUI.Opponent.PLAYER);
            }
        });

        this.botEasy = new JButton("Easy bot");
        this.botEasy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.launchGame(TicTacToeGUI.Opponent.BOT_EASY);
            }
        });

        this.botNormal = new JButton("Normal bot");
        this.botNormal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.launchGame(TicTacToeGUI.Opponent.BOT_NORMAL);

            }
        });

        this.botHard = new JButton("Hard bot");
        this.botHard.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.launchGame(TicTacToeGUI.Opponent.BOT_HARD);
            }
        });

        this.player2.setBackground(new Color(27, 119, 255));
        this.player2.setForeground(new Color(255,255,255));

        this.botEasy.setBackground(new Color(27, 119, 255));
        this.botEasy.setForeground(new Color(255,255,255));


        this.botNormal.setBackground(new Color(27, 119, 255));
        this.botNormal.setForeground(new Color(255,255,255));


        this.botHard.setBackground(new Color(27, 119, 255));
        this.botHard.setForeground(new Color(255,255,255));



        this.pane.add(this.player2);
        this.pane.add(this.botEasy);
        this.pane.add(this.botNormal);
        this.pane.add(this.botHard);
    }

}
