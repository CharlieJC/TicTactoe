package me.charlie.TicTacToeOld;

import javax.swing.*;

/**
 * Created by Charlie on 2/20/2017.
 */
public class TTTButton extends JButton {
    private int X;
    private int Y;

    public TTTButton(int x, int y) {
        super();
        X = x;
        Y = y;
    }

    public int getPosX() {
        return X;
    }

    public int getPosY() {
        return Y;
    }
}
