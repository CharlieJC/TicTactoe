package me.charlie.TTT.GameObjects;

import me.charlie.TTT.Windows.GameFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Charlie on 2/22/2017.
 */
public class MenuBar extends JMenuBar {
    private JMenu options;
    private JMenuItem quit;
    private JMenuItem newGame;
    private GameFrame gameFrame;

    public MenuBar(GameFrame gameFrame) {
        super();
        this.gameFrame = gameFrame;
        onEnable();
    }

    private void onEnable() {

        this.options = new JMenu("Options");
        this.quit = new JMenuItem("Quit");
        this.quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gameFrame.dispose();
            }
        });

        this.newGame = new JMenuItem("Reset");
        this.newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gameFrame.reset();
            }
        });

        this.options.add(newGame);
        this.options.add(quit);

        this.add(options);

    }
}
