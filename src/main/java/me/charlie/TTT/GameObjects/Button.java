package me.charlie.TTT.GameObjects;

import me.charlie.TTT.Windows.GameFrame;
import me.charlie.TTT.Player.Team;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static me.charlie.TTT.Player.Team.NONE;
import static me.charlie.TTT.Player.Team.O;
import static me.charlie.TTT.Player.Team.X;

/**
 * Created by Charlie on 2/22/2017.
 */
public class Button extends JButton {
    private final int posX;
    private final int posY;
    private Team player;
    private final GameFrame gameFrame;
    private final ActionListener action;

    public Button(int x, int y, GameFrame gameFrame) {
        super();
        this.posX = x;
        this.posY = y;
        this.player = NONE;
        this.gameFrame = gameFrame;

        this.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));
        this.setForeground(Color.WHITE);

        this.action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (gameFrame.hasStarted() && !gameFrame.hasWinner()) {
                    gameFrame.movePlayer(new Point(posX, posY));
                }
            }
        };

        this.addActionListener(action);
    }

    public boolean isOccupied () {
        return player == X || player == O;
    }

    public void setOwner(Team team) {
        this.player = team;
        switch (team) {
            case X:
                this.setText(team.name());
                this.setBackground(new Color(0,255,0));
                this.setBorder(new BevelBorder(BevelBorder.RAISED));
                break;
            case O:
                this.setText(team.name());
                this.setBackground(new Color(255,0,0));
                this.setBorder(new BevelBorder(BevelBorder.RAISED));
                break;
            case NONE:
                this.setText("");
                this.setBackground(null);
                this.setBorder(new BevelBorder(BevelBorder.LOWERED));
                break;
        }
    }


    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public Team getPlayer() {
        return player;
    }
}
