package me.charlie.TTT.Server.server;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import me.charlie.TTT.Server.Server;

/**
 * Created by Charlie on 11-Jun-17.
 */
public class ServerHandler extends SimpleChannelInboundHandler<String> {

    private final Server server;
    private static ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    public ServerHandler(Server server) {
        this.server = server;
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
//        Channel incoming = ctx.channel();
//        for (Channel channel : channels) {
//            channel.writeAndFlush(ChatColours.RED + "[SERVER] - " + incoming.remoteAddress() + " has joined!\n" + ChatColours.RESET);
//        }
//        channels.add(ctx.channel());
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
//        Channel incoming = ctx.channel();
//        for (Channel channel : channels) {
//            channel.writeAndFlush(ChatColours.RED + "[SERVER] - " + usernames.get(incoming.remoteAddress().toString()) + " has left!\n" + ChatColours.RESET);
//            usernames.remove(incoming.remoteAddress().toString());
//        }
//        channels.remove(ctx.channel());
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, String message) throws Exception {
        Channel incoming = channelHandlerContext.channel();
        if (message.startsWith("CONNECT")) {
            String username = message.split(" ")[1];
            String address = incoming.remoteAddress().toString();
            if (!server.getUsers().containsKey(address)) {
                server.getUsers().put(address,username);
            }
        }

//        if (message.startsWith("CONNECT")) {
//            String username = message.split(" ")[1];
//
//            if (usernames.containsKey(incoming.remoteAddress().toString())) return;
//
//            usernames.put(incoming.remoteAddress().toString(), username);
//            for (Channel channel : channels) {
//                channel.writeAndFlush(ChatColours.RED + "[SERVER] - " + username + " has joined!\n" + ChatColours.RESET);
//            }
//        } else {
//            for (Channel channel : channels) {
//                if (channel != incoming) {
//                    channel.writeAndFlush(ChatColours.YELLOW + "[" + usernames.get(incoming.remoteAddress().toString()) + "] " + message + "\n" + ChatColours.RESET);
//                }
//            }
//        }
    }
}
