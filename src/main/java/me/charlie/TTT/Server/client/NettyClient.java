package me.charlie.TTT.Server.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * Created by Charlie on 11-Jun-17.
 */
public class NettyClient {
    private final String host;
    private final int port;
    public NettyClient(String host, int port,String name) throws InterruptedException {
        this.host = host;
        this.port = port;

        EventLoopGroup group = new NioEventLoopGroup();

        try {
            Bootstrap bootStrap = new Bootstrap()
                    .group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ClientInitializer());

            Channel channel = bootStrap.connect(host, port).sync().channel();
            channel.writeAndFlush("CONNECT "+ name);
            channel.writeAndFlush("\r\n");
        } finally {
            group.shutdownGracefully();
        }
    }
}
