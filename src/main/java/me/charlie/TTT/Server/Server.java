package me.charlie.TTT.Server;

import me.charlie.TTT.Game.Game;
import me.charlie.TTT.Game.GameType;
import me.charlie.TTT.Player.Human;
import me.charlie.TTT.Server.server.NettyServer;

import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Charlie on 3/21/2017.
 */
public class Server {
    private Map<String,String> users;
    private List<Game> games;
    private ServerSocket socket;

    public Server(int port) {
        games = new ArrayList<>();

        try {
            new NettyServer(port,this).run();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public Map<String, String> getUsers() {
        return users;
    }

    public void connect(String name, String address) {
        users.put(address,name);
    }

    public void play(String name, GameType type) {
        findAvailable(type).connect(new Human(name));
    }

    private Game findAvailable(GameType type) {
        for (Game game:games) {
            if (game.getGameType() == type) {
                if (!game.isFull()) {
                    return game;
                }
            }
        }
        games.add(new Game(type));
        return findAvailable(type);
    }
}
