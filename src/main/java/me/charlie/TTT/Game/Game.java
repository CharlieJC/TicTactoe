package me.charlie.TTT.Game;

import me.charlie.TTT.GameObjects.Point;
import me.charlie.TTT.Minimax.PointAndScore;
import me.charlie.TTT.Player.MultiPlayer;
import me.charlie.TTT.Player.Player;
import me.charlie.TTT.Player.Team;

import java.util.ArrayList;
import java.util.List;

import static me.charlie.TTT.Player.Team.O;
import static me.charlie.TTT.Player.Team.X;

/**
 * Created by Charlie on 3/7/2017.
 */
public class Game {
    private List<Player> connectedPlayers;
    private Player whosTurn;
    private GameState gameState;
    private GameType gameType;
    private Player[][] gameBoard;
    private Team[][] aiFakeBoard;
    private List<PointAndScore> aiScores;

    public Game (GameType type) {
        this.gameState = GameState.WAITING;
        this.connectedPlayers = new ArrayList<>();
        this.gameBoard = new Player[3][3];
        this.aiFakeBoard = new Team[3][3];
        this.gameType = type;
    }

    public GameType getGameType() {
        return gameType;
    }

    private void init() {
        for (int i = 0; i<3; i++) {
            for (int j = 0; j<3; j++) {
                this.gameBoard[i][j] = null;
                this.aiFakeBoard[i][j] = Team.NONE;
            }
        }
    }
    public boolean isFull() {
        if (this.connectedPlayers.size() == 2) return true;
        if (this.connectedPlayers.size() == 1) {
            if (this.connectedPlayers.get(0) instanceof MultiPlayer) return true;
        }
        return false;
    }

    public void connect(Player player) {
        onJoin(player);

        if (this.connectedPlayers.size() == 2) {
            this.start();
        } else if (player instanceof MultiPlayer) {
            this.whosTurn = player;
            this.start();
        }
    }

    public Player getWhosTurn() {
        return whosTurn;
    }

    private void onJoin (Player player) {
        if (this.connectedPlayers.size() == 2) {
            System.out.println("Game full");
            return;
        }

        this.connectedPlayers.add(player);

        if (this.whosTurn == null) {
            player.setTeam(X);
        } else {
            player.setTeam(O);
        }
    }

    private void start() {
        this.gameState = GameState.PLAYING;
        for (Player p : connectedPlayers) {
            p.initGame(this);
        }
    }

    public List<Player> getPlayers() {
        return connectedPlayers;
    }

    public Player[][] getGameBoard() {
        return gameBoard;
    }

    public void setPlayer(Player player, Point point) {
        if (this.gameBoard[point.getX()][point.getY()] != null) return;
        this.gameBoard[point.getX()][point.getY()] = player;
        this.aiFakeBoard[point.getX()][point.getY()] = player.getSide();

    }

    public void updatePlayers() {
        for (Player player : connectedPlayers) {
            if (gameType == GameType.REGULAR)
                player.update();
        }
    }

    private void setupCleanGame() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.gameBoard[i][j] = null;
                this.aiFakeBoard[i][j] = Team.NONE;
            }
        }
    }

    public void reset() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.gameBoard[i][j] = null;
                this.aiFakeBoard[i][j] = Team.NONE;
            }
        }
    }
    private List<Point> getAvaliablePoints() {
        List<Point> avaliablePoints = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (aiFakeBoard[i][j] == Team.NONE) {
                    avaliablePoints.add(new Point(i, j));
                }
            }
        }
        return avaliablePoints;
    }


    private int returnMin(List<Integer> list) {
        int min = Integer.MAX_VALUE;
        int index = -1;
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i) < min) {
                min = list.get(i);
                index = i;
            }
        }
        return list.get(index);
    }

    private int returnMax(List<Integer> list) {
        int max = Integer.MIN_VALUE;
        int index = -1;
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i) > max) {
                max = list.get(i);
                index = i;
            }
        }
        return list.get(index);
    }

    public void callMinimax(int depth, Team turn) {
        aiScores = new ArrayList<>();
        minimax(depth, turn);
    }

    public Point returnBestMove() {
        int MAX = -100000;
        int best = -1;

        for (int i = 0; i < aiScores.size(); ++i) {
            if (MAX < aiScores.get(i).getScore()) {
                MAX = aiScores.get(i).getScore();
                best = i;
            }
        }

        return aiScores.get(best).getPoint();
    }

    private int minimax(int depth, Team player) {

        List<Point> pointsAvailable = getAvaliablePoints();


        if (hasXAIWon()) {
            return -1;
        } else if (hasOAIWon()) {
            return 1;
        } else if (pointsAvailable.isEmpty()) {
            return 0;
        }

        List<Integer> scores = new ArrayList<>();

        for (int i = 0; i < pointsAvailable.size(); ++i) {
            Point point = pointsAvailable.get(i);

            if (player == O) { //X's turn select the highest from below minimax() call
                placeAMoveAI(point, O);
                int currentScore = minimax(depth + 1, X);
                scores.add(currentScore);

                if (depth == 0)
                    this.aiScores.add(new PointAndScore(currentScore, point));

            } else if (player == X) {//O's turn select the lowest from below minimax() call
                placeAMoveAI(point, X);
                scores.add(minimax(depth + 1, O));
            }
            aiFakeBoard[point.getX()][point.getY()] = Team.NONE; //Reset this point
        }
        return player == O ? returnMax(scores) : returnMin(scores);
    }

    private void placeAMoveAI(Point point, Team player) {
        this.aiFakeBoard[point.getX()][point.getY()] = player;
    }

    private boolean hasXAIWon() {
        Team[][] board = this.aiFakeBoard;
        if ((board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0] == X) || (board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[0][2] == X)) {
            return true;
        }

        for (int i = 0; i < 3; ++i) {
            if (((board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] == X)
                    || (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] == X))) {
                return true;
            }
        }
        return false;
    }


    private boolean hasOAIWon() {
        Team[][] board = this.aiFakeBoard;
        if ((board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0] == O) || (board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[0][2] == O)) {
            return true;
        }
        for (int i = 0; i < 3; ++i) {
            if ((board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] == O)
                    || (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] == O)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasXWon() {
        Player[][] board = this.gameBoard;
        if ((board[0][0].getSide() == board[1][1].getSide() && board[0][0].getSide() == board[2][2].getSide() && board[0][0].getSide() == X) || (board[0][2].getSide() == board[1][1].getSide() && board[0][2].getSide() == board[2][0].getSide() && board[0][2].getSide() == X)) {
            return true;
        }

        for (int i = 0; i < 3; ++i) {
            if (((board[i][0].getSide() == board[i][1].getSide() && board[i][0].getSide() == board[i][2].getSide() && board[i][0].getSide() == X)
                     || (board[0][i].getSide() == board[1][i].getSide() && board[0][i].getSide() == board[2][i].getSide() && board[0][i].getSide() == X))) {
                return true;
            }
        }
        return false;
    }

    public boolean hasOWon() {
        Player[][] board = this.gameBoard;
        if ((board[0][0].getSide() == board[1][1].getSide() && board[0][0].getSide() == board[2][2].getSide() && board[0][0].getSide() == O) || (board[0][2].getSide() == board[1][1].getSide() && board[0][2].getSide() == board[2][0].getSide() && board[0][2].getSide() == O)) {
            return true;
        }
        for (int i = 0; i < 3; ++i) {
            if ((board[i][0].getSide() == board[i][1].getSide() && board[i][0].getSide() == board[i][2].getSide() && board[i][0].getSide() == O)
                    || (board[0][i].getSide() == board[1][i].getSide() && board[0][i].getSide() == board[2][i].getSide() && board[0][i].getSide() == O)) {
                return true;
            }
        }
        return false;
    }
}
