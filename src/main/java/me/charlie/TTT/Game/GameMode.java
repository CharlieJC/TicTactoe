package me.charlie.TTT.Game;

/**
 * Created by Charlie on 17/05/16.
 */
public enum GameMode {
    MULTI, SINGLE, BOT_EASY, BOT_NORMAL, BOT_HARD
}
