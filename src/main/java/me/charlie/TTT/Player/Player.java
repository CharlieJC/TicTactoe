package me.charlie.TTT.Player;

import me.charlie.TTT.Game.Game;
import me.charlie.TTT.GameObjects.Point;

import java.awt.*;

/**
 * Created by Charlie on 2/22/2017.
 */
public interface Player {
    String getName();
    void move(Point point);
    void giveUp();
    Team getSide();
    void setTeam(Team team);
    void initGame(Game game);
    void sendMessage(String message,Color colour);
    void update();
}
