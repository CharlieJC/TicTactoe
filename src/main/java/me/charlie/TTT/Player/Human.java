package me.charlie.TTT.Player;


import me.charlie.TTT.Game.Game;
import me.charlie.TTT.GameObjects.Point;
import me.charlie.TTT.Windows.GameFrame;

import java.awt.*;

/**
 * Created by Charlie on 2/22/2017.
 */
public class Human implements Player {

    private String name;
    private Team side;
    private GameFrame gameFrame;

    public Human(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void move(Point point) {
        gameFrame.movePlayer(point);
    }

    @Override
    public void giveUp() {

    }

    @Override
    public Team getSide() {
        return this.side;
    }

    @Override
    public void setTeam(Team team) {
        this.side = team;
    }

    @Override
    public void initGame(Game game) {
        this.gameFrame = new GameFrame(game);
        this.gameFrame.addPlayer(this);
    }

    @Override
    public void sendMessage(String message,Color colour) {
        gameFrame.getMessageBar().setTextColor(colour);
        gameFrame.getMessageBar().setMessage(message);
    }

    GameFrame getGameFrame() {
        return gameFrame;
    }
    @Override
    public void update() {

    }
}
