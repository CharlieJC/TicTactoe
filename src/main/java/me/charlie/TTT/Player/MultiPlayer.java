package me.charlie.TTT.Player;

import me.charlie.TTT.Game.Game;
import me.charlie.TTT.GameObjects.Point;
import me.charlie.TTT.Windows.GameFrame;

import java.awt.*;

/**
 * Created by Charlie on 3/21/2017.
 */
public class MultiPlayer implements Player {

    private Team side;
    private GameFrame gameFrame;

    public MultiPlayer() {
        this.side = Team.X;
    }

    @Override
    public String getName() {
        return side.name();
    }

    @Override
    public void move(Point point) {
        gameFrame.getBoard().setPlayer(this, point);
        if (this.gameFrame.hasWinner()) return;
        if (this.gameFrame.getBoard().hasXWon()) {
            sendMessage("X has won!",Color.WHITE);
            gameFrame.setWinner(true);
            return;
        } else if (this.gameFrame.getBoard().hasOWon()) {
            sendMessage("O has won!",Color.WHITE);
            gameFrame.setWinner(true);
            return;
        }

        switchSide();
    }

    @Override
    public void giveUp() {

    }

    @Override
    public Team getSide() {
        return this.side;
    }

    @Override
    public void setTeam(Team team) {
        this.side = team;
    }

    @Override
    public void initGame(Game game) {
        this.gameFrame = new GameFrame(game);
        this.gameFrame.addPlayer(this);
    }

    private void switchSide() {
        if (this.side == Team.X) {
            setTeam(Team.O);
            sendMessage(Team.O.name() + "'s Turn!", Color.WHITE);

        } else {
            setTeam(Team.X);
            sendMessage(Team.X.name() + "'s Turn!", Color.WHITE);

        }
    }

    @Override
    public void sendMessage(String message, Color colour) {
        gameFrame.getMessageBar().setTextColor(colour);
        gameFrame.getMessageBar().setMessage(message);
    }

    @Override
    public void update() {

    }
}

