package me.charlie.TTT.Player;

import me.charlie.TTT.Game.Game;
import me.charlie.TTT.GameObjects.Point;
import me.charlie.TTT.Windows.GameFrame;

import java.awt.*;


/**
 * Created by Charlie on 2/22/2017.
 */
public class Bot implements Player{

    private String name;
    private Team side;
    private GameFrame gameFrame;

    public Bot() {
        this.name = "Bot";
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void move(Point point) {
        simulateMove();
    }

    @Override
    public void giveUp() {

    }

    @Override
    public Team getSide() {
        return this.side;
    }

    @Override
    public void setTeam(Team team) {
        this.side = team;
    }

    private Point simulateMove() {

        gameFrame.getBoard().callMinimax(0, side);
        return gameFrame.getBoard().returnBestMove();
    }

    @Override
    public void initGame(Game game) {
        this.gameFrame = ((Human)game.getWhosTurn()).getGameFrame();
    }

    @Override
    public void sendMessage(String message,Color colour) {

    }
    @Override
    public void update() {

    }
}

