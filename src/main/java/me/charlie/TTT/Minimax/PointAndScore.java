package me.charlie.TTT.Minimax;

import me.charlie.TTT.GameObjects.Point;

/**
 * Created by Charlie on 2/25/2017.
 */
public final class PointAndScore {
    private final int score;
    private final Point point;

    public PointAndScore(int score, Point point) {
        this.score = score;
        this.point = point;
    }

    public int getScore() {
        return score;
    }

    public Point getPoint() {
        return point;
    }
}
