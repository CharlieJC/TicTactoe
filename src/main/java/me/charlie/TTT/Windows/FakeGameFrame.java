package me.charlie.TTT.Windows;

import me.charlie.TTT.Game.Game;

/**
 * Created by Charlie on 3/8/2017.
 */
public class FakeGameFrame extends GameFrame {

    public FakeGameFrame(Game game) {
        super(game);
        this.setVisible(false);
    }
}
