package me.charlie.TTT.Windows.Panels;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Charlie on 2/24/2017.
 */
public class MainMenuMessageBox extends JPanel {
    private JLabel jLabel;
    public MainMenuMessageBox(String defaultMessage) {
        super(new FlowLayout());
        this.jLabel = new JLabel(defaultMessage);
        this.jLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        this.add(this.jLabel);
        this.setBackground(new Color(14, 112, 255));
        this.setPreferredSize(new Dimension(50,150));
    }

    void setMessage(String message) {
        this.jLabel.setText(message);
    }

    void setTextColor(Color color) {
        this.jLabel.setForeground(color);
    }
}
