package me.charlie.TTT.Windows.Panels;

/**
 * Created by Charlie on 17/05/16.
 */

import me.charlie.TTT.Game.Game;
import me.charlie.TTT.Game.GameType;
import me.charlie.TTT.Player.Bot;
import me.charlie.TTT.Player.Human;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Charlie on 2/24/2017.
 */

class PickOpponent extends JFrame {

    private Container pane;
    private JButton botEasy;
    private JButton botNormal;
    private JButton botHard;

    PickOpponent() {

        super("Choose your opponent");
        this.pane = this.getContentPane();
        this.setLayout(new FlowLayout());
        this.setSize(150, 100);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setResizable(false);
        this.setUndecorated(true);
        this.setVisible(true);
        pane.setBackground(new Color(27, 119, 255));

        initButtons();

    }

    private void initButtons() {

        this.botEasy = new JButton("Easy bot");
        this.botEasy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                Game game = new Game(GameType.BOT_EASY);
                game.connect(new Human("Test1"));
                game.connect(new Bot());
            }
        });

        this.botNormal = new JButton("Normal bot");
        this.botNormal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        this.botHard = new JButton("Hard bot");
        this.botHard.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        this.botEasy.setBackground(new Color(27, 119, 255));
        this.botEasy.setForeground(new Color(255, 255, 255));


        this.botNormal.setBackground(new Color(27, 119, 255));
        this.botNormal.setForeground(new Color(255, 255, 255));


        this.botHard.setBackground(new Color(27, 119, 255));
        this.botHard.setForeground(new Color(255, 255, 255));


        this.pane.add(this.botEasy);
        this.pane.add(this.botNormal);
        this.pane.add(this.botHard);
    }
}
