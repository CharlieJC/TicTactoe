package me.charlie.TTT.Windows.Panels;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.SoftBevelBorder;
import java.awt.*;

/**
 * Created by Charlie on 2/22/2017.
 */
public class GameFrameMessageBox extends JPanel {
    private JLabel jLabel;
    private Border border;
    public GameFrameMessageBox(String defaultMessage) {
        super(new FlowLayout());
        this.jLabel = new JLabel(defaultMessage);
        this.jLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 30));
        this.add(this.jLabel);
        this.border = BorderFactory.createBevelBorder(SoftBevelBorder.RAISED,Color.WHITE,Color.WHITE);
        this.setBackground(new Color(14, 112, 255));
        this.setBorder(this.border);
        this.setPreferredSize(new Dimension(400,100));
    }

    public void setMessage(String message) {
        this.jLabel.setText(message);
    }

    public void setTextColor(Color color) {
        this.jLabel.setForeground(color);
    }
}
