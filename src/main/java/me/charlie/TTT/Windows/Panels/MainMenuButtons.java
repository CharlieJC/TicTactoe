package me.charlie.TTT.Windows.Panels;

import me.charlie.TTT.Game.Game;
import me.charlie.TTT.Game.GameType;
import me.charlie.TTT.Player.MultiPlayer;
import me.charlie.TTT.Windows.MainMenu;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Charlie on 2/24/2017.
 */
public class MainMenuButtons extends JPanel {

    private MainMenu mainMenu;
    private boolean isOnline;

    public MainMenuButtons(MainMenu mainMenu,boolean isOnline) {
        super();
        this.mainMenu = mainMenu;
        this.setLayout(new GridBagLayout());
        this.setBackground(new Color(14, 112, 255));
        this.setPreferredSize(new Dimension(300, 800));
        this.initButtons();
        this.isOnline = isOnline;

    }

    private void initButtons() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0,0,20,0);

        mainMenu.getMessageBox().setTextColor(new Color(255, 0, 0));

        MenuButton play = new MenuButton("Regular");
        play.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Game game = new Game(GameType.REGULAR);
                game.connect(new MultiPlayer());
            }
        });

        this.add(Box.createRigidArea(new Dimension(0, 10)), c);

        MenuButton bots = new MenuButton("Bots");
        bots.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new PickOpponent();
            }
        });

        this.add(Box.createRigidArea(new Dimension(0, 10)), c);

        MenuButton multi = new MenuButton("Multiplayer");
        multi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainMenu.getMessageBox().setMessage("This option is currently disabled!");
                cleanLabel();
                if (!isOnline) {
                    mainMenu.getMessageBox().setMessage("You are not connected to the main server!");
                    cleanLabel();
                }
            }
        });

        this.add(Box.createRigidArea(new Dimension(0, 10)), c);

        MenuButton quit = new MenuButton("Quit");
        quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainMenu.dispose();
            }
        });

        this.add(play, c);
        this.add(bots, c);
        this.add(multi, c);
        this.add(quit, c);

    }

    private void cleanLabel() {
        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainMenu.getMessageBox().setMessage("");
            }
        };

        Timer delayedTask = new Timer(5000, listener);
        delayedTask.setRepeats(false);
        delayedTask.start();
    }

    private class MenuButton extends JButton {

        MenuButton(String text) {
            super();
            this.setText(text);
            this.setForeground(new Color(255, 255, 255));
            this.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 40));
            this.setPreferredSize(new Dimension(100, 250));
            this.setBackground(new Color(0, 255, 255));
            this.setBorder(new BevelBorder(BevelBorder.RAISED));
            this.setAlignmentX(Component.CENTER_ALIGNMENT);
        }
    }

}
