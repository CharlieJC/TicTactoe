package me.charlie.TTT.Windows.Panels;

import me.charlie.TTT.GameObjects.Point;
import me.charlie.TTT.Minimax.PointAndScore;
import me.charlie.TTT.Player.Player;
import me.charlie.TTT.Player.Team;
import me.charlie.TTT.Windows.GameFrame;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.SoftBevelBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static me.charlie.TTT.Player.Team.O;
import static me.charlie.TTT.Player.Team.X;

/**
 * Created by Charlie on 2/22/2017.
 */
public class GameFrameBoard extends JPanel {
    private final int length = 3;
    private me.charlie.TTT.GameObjects.Button[][] icons;
    private Team[][] aiFakeBoard;
    private GameFrame gameFrame;
    private Border border;
    private List<PointAndScore> pointsAndScores;

    public GameFrameBoard(GameFrame gameFrame) {
        super(new GridLayout(3, 3));
        this.gameFrame = gameFrame;
        this.icons = new me.charlie.TTT.GameObjects.Button[length][length];
        this.aiFakeBoard = new Team[length][length];
        this.border = BorderFactory.createBevelBorder(SoftBevelBorder.RAISED, Color.WHITE, Color.WHITE);
        this.setBorder(this.border);
        this.setPreferredSize(new Dimension(300, 300));
        this.setupCleanGame();
    }

    public void setPlayer(Player player, Point point) {
        if (this.icons[point.getX()][point.getY()].isOccupied()) return;

        me.charlie.TTT.GameObjects.Button old = this.icons[point.getX()][point.getY()];
        old.setOwner(player.getSide());
        this.icons[point.getX()][point.getY()] = old;
        this.aiFakeBoard[point.getX()][point.getY()] = player.getSide();
        refresh();

    }

    private void setupCleanGame() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                me.charlie.TTT.GameObjects.Button icon = new me.charlie.TTT.GameObjects.Button(i, j, this.gameFrame);
                icon.setOwner(Team.NONE);
                icon.setBackground(new Color(14, 112, 255));
                icon.setBorder(new BevelBorder(BevelBorder.LOWERED));
                this.icons[i][j] = icon;
                this.aiFakeBoard[i][j] = Team.NONE;

            }
        }
        refresh();
    }


    private void refresh() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.add(icons[i][j]);
            }
        }
    }

    public void reset() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.icons[i][j].setOwner(Team.NONE);
                this.icons[i][j].setBackground(new Color(14, 112, 255));
                this.icons[i][j].setBorder(new BevelBorder(BevelBorder.LOWERED));
                this.aiFakeBoard[i][j] = Team.NONE;
            }
        }
    }
    private List<Point> getAvaliablePoints() {
        List<Point> avaliablePoints = new ArrayList<>();



        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (aiFakeBoard[i][j] == Team.NONE) {
                    avaliablePoints.add(new Point(i, j));
                }
            }
        }
        return avaliablePoints;
    }


    private int returnMin(List<Integer> list) {
        int min = Integer.MAX_VALUE;
        int index = -1;
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i) < min) {
                min = list.get(i);
                index = i;
            }
        }
        return list.get(index);
    }

    private int returnMax(List<Integer> list) {
        int max = Integer.MIN_VALUE;
        int index = -1;
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i) > max) {
                max = list.get(i);
                index = i;
            }
        }
        return list.get(index);
    }

    public void callMinimax(int depth, Team turn) {
        pointsAndScores = new ArrayList<>();
        minimax(depth, turn);
    }

    public Point returnBestMove() {
        int MAX = -100000;
        int best = -1;

        for (int i = 0; i < pointsAndScores.size(); ++i) {
            if (MAX < pointsAndScores.get(i).getScore()) {
                MAX = pointsAndScores.get(i).getScore();
                best = i;
            }
        }

        return pointsAndScores.get(best).getPoint();
    }

    private int minimax(int depth, Team player) {

        List<Point> pointsAvailable = getAvaliablePoints();


        if (hasXAIWon()) {
            return -1;
        } else if (hasOAIWon()) {
            return 1;
        } else if (pointsAvailable.isEmpty()) {
            return 0;
        }

        List<Integer> scores = new ArrayList<>();

        for (int i = 0; i < pointsAvailable.size(); ++i) {
            Point point = pointsAvailable.get(i);

            if (player == O) { //X's turn select the highest from below minimax() call
                placeAMoveAI(point, O);
                int currentScore = minimax(depth + 1, X);
                scores.add(currentScore);

                if (depth == 0)
                    pointsAndScores.add(new PointAndScore(currentScore, point));

            } else if (player == X) {//O's turn select the lowest from below minimax() call
                placeAMoveAI(point, X);
                scores.add(minimax(depth + 1, O));
            }
            aiFakeBoard[point.getX()][point.getY()] = Team.NONE; //Reset this point
        }
        return player == O ? returnMax(scores) : returnMin(scores);
    }

    private void placeAMoveAI(Point point, Team player) {
        aiFakeBoard[point.getX()][point.getY()] = player;
    }

    private boolean hasXAIWon() {
        Team[][] board = this.aiFakeBoard;
        if ((board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0] == Team.X) || (board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[0][2] == X)) {
            return true;
        }

        for (int i = 0; i < 3; ++i) {
            if (((board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] == Team.X)
                    || (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] == Team.X))) {
                return true;
            }
        }
        return false;
    }


    private boolean hasOAIWon() {
        Team[][] board = this.aiFakeBoard;
        if ((board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0] == Team.O) || (board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[0][2] == O)) {
            return true;
        }
        for (int i = 0; i < 3; ++i) {
            if ((board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] == Team.O)
                    || (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] == Team.O)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasXWon() {
        me.charlie.TTT.GameObjects.Button[][] board = this.icons;
        if ((board[0][0].getPlayer() == board[1][1].getPlayer() && board[0][0].getPlayer() == board[2][2].getPlayer() && board[0][0].getPlayer() == Team.X) || (board[0][2].getPlayer() == board[1][1].getPlayer() && board[0][2].getPlayer() == board[2][0].getPlayer() && board[0][2].getPlayer() == X)) {
            return true;
        }

        for (int i = 0; i < 3; ++i) {
            if (((board[i][0].getPlayer() == board[i][1].getPlayer() && board[i][0].getPlayer() == board[i][2].getPlayer() && board[i][0].getPlayer() == Team.X)
                    || (board[0][i].getPlayer() == board[1][i].getPlayer() && board[0][i].getPlayer() == board[2][i].getPlayer() && board[0][i].getPlayer() == Team.X))) {
                return true;
            }
        }
        return false;
    }

    public boolean hasOWon() {
        me.charlie.TTT.GameObjects.Button[][] board = this.icons;
        if ((board[0][0].getPlayer() == board[1][1].getPlayer() && board[0][0].getPlayer() == board[2][2].getPlayer() && board[0][0].getPlayer() == Team.O) || (board[0][2].getPlayer() == board[1][1].getPlayer() && board[0][2].getPlayer() == board[2][0].getPlayer() && board[0][2].getPlayer() == O)) {
            return true;
        }
        for (int i = 0; i < 3; ++i) {
            if ((board[i][0].getPlayer() == board[i][1].getPlayer() && board[i][0].getPlayer() == board[i][2].getPlayer() && board[i][0].getPlayer() == Team.O)
                    || (board[0][i].getPlayer() == board[1][i].getPlayer() && board[0][i].getPlayer() == board[2][i].getPlayer() && board[0][i].getPlayer() == Team.O)) {
                return true;
            }
        }
        return false;
    }
}
