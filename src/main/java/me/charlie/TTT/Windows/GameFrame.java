package me.charlie.TTT.Windows;

import me.charlie.TTT.Game.Game;
import me.charlie.TTT.GameObjects.MenuBar;
import me.charlie.TTT.GameObjects.Point;
import me.charlie.TTT.Player.MultiPlayer;
import me.charlie.TTT.Player.Player;
import me.charlie.TTT.Windows.Panels.GameFrameBoard;
import me.charlie.TTT.Windows.Panels.GameFrameMessageBox;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Charlie on 2/22/2017.
 */
public class GameFrame extends JFrame {
    private GameFrameBoard board;
    private GameFrameMessageBox messageBar;
    private MenuBar menuBar;
    private boolean hasWinner;
    private List<Player> players;
    private Player current;
    private int turn;
    private Container pane;

    public GameFrame(Game game) {
        super("Tic Tac Toe");
        this.board = new GameFrameBoard(this);
        this.players = new ArrayList<>();
        this.hasWinner = false;
        this.turn = 0;
        this.current = game.getWhosTurn();
        this.messageBar = new GameFrameMessageBox(getCurrent().getName() + " begins!");
        this.onEnable();
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public void movePlayer(Point point) {
        this.getCurrent().move(point);
    }


    private void onEnable() {
        this.pane = this.getContentPane();
        pane.setBackground(new Color(14, 112, 255));
        this.setSize(500, 500);
        this.setResizable(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = GridBagConstraints.REMAINDER;

        c.insets = new Insets(5,0,5,0);

        pane.add(board,c);

        c.gridy = 50;
        pane.add(messageBar,c);
        this.messageBar.setTextColor(new Color(255,255,255));

        this.setBackground(new Color(14, 112, 255));


        this.menuBar = new MenuBar(this);
        this.setJMenuBar(this.menuBar);

        this.pack();

        this.setLocationRelativeTo(null);

        this.setMinimumSize(new Dimension(500,500));

        this.setVisible(true);
    }

    public void reset() {
        this.messageBar.setMessage(getCurrent().getName() + " begins!");
        this.board.reset();
        this.setWinner(false);
    }

    private Player getCurrent() {
        return this.current;
    }

    public boolean hasStarted() {
        return players.size() == 2 || players.get(0) instanceof MultiPlayer;
    }

    public GameFrameBoard getBoard() {
        return board;
    }

    public GameFrameMessageBox getMessageBar() {
        return messageBar;
    }

    public boolean hasWinner() {
        return hasWinner;
    }

    public void setWinner(boolean hasWinner) {
        this.hasWinner = hasWinner;
    }
}
