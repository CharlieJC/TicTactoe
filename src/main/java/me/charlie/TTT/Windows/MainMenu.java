package me.charlie.TTT.Windows;

import me.charlie.TTT.Windows.Panels.MainMenuButtons;
import me.charlie.TTT.Windows.Panels.MainMenuMessageBox;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Charlie on 2/24/2017.
 */
public class MainMenu extends JFrame {

    private Container pane;
    private MainMenuMessageBox messageBox;
    private boolean onlineMode;

    public MainMenu(boolean isOnline) {
        super("Main Menu");
        this.pane = this.getContentPane();
        pane.setBackground(new Color(14, 112, 255));
        this.setSize(500, 500);
        this.setResizable(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.messageBox = new MainMenuMessageBox("");

        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.fill = GridBagConstraints.BOTH;
        this.add(new MainMenuButtons(this,isOnline), c);
        this.add(messageBox, c);

        this.setVisible(true);
    }

    public MainMenuMessageBox getMessageBox() {
        return messageBox;
    }
}
